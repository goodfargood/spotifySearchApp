"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
const core_1 = require("@angular/core");
const core_2 = require("@angular/core");
const platform_browser_dynamic_1 = require("@angular/platform-browser-dynamic");
const platform_browser_1 = require("@angular/platform-browser");
const http_1 = require("@angular/http");
const router_1 = require("@angular/router");
const core_3 = require("@angular/core");
const forms_1 = require('@angular/forms');
const common_1 = require("@angular/common");
require('rxjs/Rx');
let SpotifySearch = class SpotifySearch {
};
SpotifySearch = __decorate([
    core_1.Component({
        selector: "spotify-search",
        template: `
          <router-outlet></router-outlet>
          `
    }), 
    __metadata('design:paramtypes', [])
], SpotifySearch);
let SpotifyService_1 = class SpotifyService {
    constructor(http) {
        this.http = http;
    }
    query(url, params) {
        let queryUrl = `${SpotifyService_1.URL_BASE}${url}`;
        if (params) {
            queryUrl = `${queryUrl}?${params.join('&')}`;
        }
        return this.http.request(queryUrl).map((res) => res.json());
    }
    search(query, type) {
        return this.query('/search', [`q=${query}`, `type=${type}`]);
    }
    searchTrack(query) {
        return this.search(query, 'track');
    }
    getTrack(id) {
        return this.query(`/tracks/${id}`);
    }
    getArtist(id) {
        return this.query(`/artist/${id}`);
    }
    getAlbum(id) {
        return this.query(`/albums/${id}`);
    }
};
let SpotifyService = SpotifyService_1;
SpotifyService.URL_BASE = "https://api.spotify.com/v1";
SpotifyService = SpotifyService_1 = __decorate([
    core_3.Injectable(), 
    __metadata('design:paramtypes', [http_1.Http])
], SpotifyService);
let SearchCom = class SearchCom {
    constructor(spoti, router, route) {
        this.spoti = spoti;
        this.router = router;
        this.route = route;
        this.route.queryParams.subscribe(params => { this.query = params['query'] || ''; });
    }
    ngOnInit() {
        this.search();
    }
    submit(query) {
        this.router.navigate(['search'], { queryParams: { query: query } })
            .then(_ => this.search());
    }
    search() {
        console.log("this.query", this.query);
        if (!this.query) {
            return;
        }
        this.spoti.searchTrack(this.query).subscribe((res) => this.renderResults(res));
    }
    renderResults(res) {
        this.results = null;
        if (res && res.tracks && res.tracks.items) {
            this.results = res.tracks.items;
        }
    }
};
SearchCom = __decorate([
    core_1.Component({
        selector: 'search',
        template: `
          <h1>search</h1>
          <p>
               <input type="text" #newquery (keydown.enter)="submit(newquery.value)" [value]="query" >
               <button (click)="submit(newquery.value)">search</button>
          </p>
          <div *ngIf="results">
               <div *ngIf="!results.length">
                    no track found!
               </div>
          <div *ngIf="results.length">
               <h1>results</h1>
               <div class="row">
                    <div class="col-sm-6 col-md-4" *ngFor="let eachRes of results">
                    <div class="thumbnail">
                         <img class="img img-responsive" src="{{eachRes.album.images[0].url}}">
                         <div class="caption">
                              <a [routerLink]="['/artist',eachRes.artists[0].id]"> {{ eachRes.artists[0].name }} </a>
                              <br>
                              <a [routerLink]="['/track',eachRes.id]"> {{ eachRes.name }}</a>
                              <br>
                              <a [routerLink]="['/albums',eachRes.album.id]"> {{ eachRes.album.name }}</a>
                         </div>
                    </div>
                    </div>
               </div>
          </div>
          `
    }), 
    __metadata('design:paramtypes', [SpotifyService, router_1.Router, router_1.ActivatedRoute])
], SearchCom);
let TrackCom = class TrackCom {
    constructor(route, spotify, location) {
        this.route = route;
        this.spotify = spotify;
        this.location = location;
        route.params.subscribe(params => { this.id = params['id']; });
    }
    ngOnInit() {
        this.spotify.getTrack(this.id).subscribe((res) => this.renderTrack(res));
    }
    back() {
        this.location.back();
    }
    renderTrack(res) {
        this.track = res;
    }
};
TrackCom = __decorate([
    core_1.Component({
        selector: 'trackk',
        template: `
          <div *ngIf="track">
               <h1>{{ track.name }}</h1>
               <img src=" track.album.images[1].url" }}>
               <audio controls src="{{ track.preview_url }}"></audio>
               <p><a href (click)="back()">Back</a></p>
               </div>
               `
    }), 
    __metadata('design:paramtypes', [router_1.ActivatedRoute, SpotifyService, common_1.Location])
], TrackCom);
let AlbumCom = class AlbumCom {
    constructor(route, spotify, location) {
        this.route = route;
        this.spotify = spotify;
        this.location = location;
        route.params.subscribe(params => { this.id = params['id']; });
    }
    ngOnInit() {
        this.spotify.getAlbum(this.id).subscribe((res) => this.renderAlbum(res));
    }
    back() {
        this.location.back();
    }
    renderAlbum(res) {
        this.album = res;
    }
};
AlbumCom = __decorate([
    core_1.Component({
        selector: "albums",
        template: `
          <div *ngIf="album">
               <h1>{{ album.name }}</h1>
               <h2>{{ album.artists[0].name }}</h2>
               <img src="{{ album.images[1].url }}">
               <h3>Tracks</h3>
               <ol>
                    <li *ngFor="let t of album.tracks.items">
                         <a [routerLink]="['/tracks',t.id]">
                         {{ t.name }}
                         </a>
                    </li>
               </ol>
               <p><a (click)="back()">Back</a></p>
          </div>
          `
    }), 
    __metadata('design:paramtypes', [router_1.ActivatedRoute, SpotifyService, common_1.Location])
], AlbumCom);
let ArtistCom = class ArtistCom {
    constructor(route, spotify, location) {
        this.route = route;
        this.spotify = spotify;
        this.location = location;
        route.params.subscribe(params => { this.id = params['id']; });
    }
    ngOnInit() {
        this.spotify
            .getArtist(this.id)
            .subscribe((res) => this.renderArtist(res));
    }
    back() {
        this.location.back();
    }
    renderArtist(res) {
        this.artist = res;
    }
};
ArtistCom = __decorate([
    core_1.Component({
        selector: 'artist',
        template: `
  <div *ngIf="artist">
    <h1>{{ artist.name }}</h1>

    <p>
      <img src="{{ artist.images[0].url }}">
    </p>

    <p><a href (click)="back()">Back</a></p>
  </div>
  `
    }), 
    __metadata('design:paramtypes', [router_1.ActivatedRoute, SpotifyService, common_1.Location])
], ArtistCom);
exports.ArtistCom = ArtistCom;
const routes = [
    { path: '', redirectTo: 'search', pathMatch: 'full' },
    { path: 'search', component: SearchCom },
    { path: 'track/:id', component: TrackCom },
    { path: 'artist/:id', component: ArtistCom },
    { path: 'albums/:id', component: AlbumCom }
];
let SpotifySearchModule = class SpotifySearchModule {
};
SpotifySearchModule = __decorate([
    core_2.NgModule({
        declarations: [TrackCom, AlbumCom, SpotifySearch, SearchCom, ArtistCom],
        imports: [platform_browser_1.BrowserModule, http_1.HttpModule, forms_1.FormsModule, router_1.RouterModule.forRoot(routes)],
        bootstrap: [SpotifySearch],
        providers: [SpotifyService, { provide: common_1.APP_BASE_HREF, useValue: '/' }, { provide: common_1.LocationStrategy, useClass: common_1.HashLocationStrategy }]
    }), 
    __metadata('design:paramtypes', [])
], SpotifySearchModule);
platform_browser_dynamic_1.platformBrowserDynamic().bootstrapModule(SpotifySearchModule);
//# sourceMappingURL=app.js.map